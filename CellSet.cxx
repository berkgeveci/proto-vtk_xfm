//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "CellSet.h"

namespace adios2
{
namespace datamodel
{

std::vector<vtkm::cont::DynamicCellSet> CellSet::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const std::vector<size_t>& blocksToRead)
{
  std::vector<vtkm::cont::DynamicArrayHandle> arrays =
    this->ReadSelf(paths, sources, blocksToRead);
  std::vector<vtkm::cont::DynamicCellSet> cellSets;
  cellSets.reserve(arrays.size());
  for(auto array: arrays)
  {
    vtkm::cont::CellSetSingleType<> cellSet("cells");
    vtkm::cont::ArrayHandle<vtkm::Id> cellSetArray =
      array.Cast<vtkm::cont::ArrayHandle<vtkm::Id> >();
    cellSet.Fill(array.GetNumberOfValues(),
                 vtkm::CELL_SHAPE_TRIANGLE,
                 3,
                 cellSetArray);
    cellSets.push_back(cellSet);
  }
  return cellSets;
}

}
}