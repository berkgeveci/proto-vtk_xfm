//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "Array.h"

namespace adios2
{
namespace datamodel
{

std::vector<vtkm::cont::DynamicArrayHandle> Array::Read(
  const std::unordered_map<std::string, std::string>& paths,
  DataSourcesType& sources,
  const std::vector<size_t>& blocksToRead)
{
  return this->ReadSelf(paths, sources, blocksToRead);
}

}
}