//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _CellSet_H_
#define _CellSet_H_

#include "DataModel.h"

#include <vtkm/cont/DynamicCellSet.h>

namespace adios2
{
namespace datamodel
{

struct CellSet : public DataModelBase
{
  std::vector<vtkm::cont::DynamicCellSet> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const std::vector<size_t>& blocksToRead);
};

}
}

#endif