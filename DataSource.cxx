//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "DataSource.h"

#include <vtkm/cont/ArrayHandle.h>
#include <vtkm/cont/Storage.h>

#include <numeric>

#include <iostream>

namespace adios2
{
namespace datamodel
{

void DataSource::OpenSource(const std::string& fname)
{
  if(this->Adios)
  {
    return;
  }

  this->Adios.reset(
    new adios2::ADIOS(MPI_COMM_WORLD, adios2::DebugON));

  this->AdiosIO = this->Adios->DeclareIO("vtkADIOS2ImageRead");
  this->AdiosIO.SetEngine("bp3");
  this->BpReader = this->AdiosIO.Open(fname, adios2::Mode::Read);

  this->AvailVars =  this->AdiosIO.AvailableVariables();
  this->AvailAtts =  this->AdiosIO.AvailableAttributes();
}

template <typename VariableType, typename VecType>
vtkm::cont::DynamicArrayHandle AllocateArrayHandle(
  size_t bufSize, VariableType*& buffer)
{
  vtkm::cont::internal::Storage<
    VecType, vtkm::cont::StorageTagBasic> storage;
  storage.Allocate(bufSize);
  buffer = reinterpret_cast<VariableType*>(storage.GetArray());
  return vtkm::cont::ArrayHandle<VecType>(std::move(storage));
}

template <typename VariableType>
vtkm::cont::DynamicArrayHandle ReadVariableInternal(
  adios2::Engine& bpReader,
  adios2::Variable<VariableType>& varADIOS2,
  size_t blockId)
{
  // TODO: Fix the time argument.
  auto blocksInfo =  bpReader.BlocksInfo(varADIOS2, 0);
  const auto& shape = blocksInfo[blockId].Count;
  size_t bufSize = 1;
  for(auto n : shape)
  {
    bufSize *= n;
  }

  vtkm::cont::DynamicArrayHandle retVal;
  VariableType* buffer;

  if (shape.size() == 1)
  {
    vtkm::cont::internal::Storage<
      VariableType, vtkm::cont::StorageTagBasic> storage;
    storage.Allocate(bufSize);
    buffer = storage.GetArray();
    vtkm::cont::ArrayHandle<VariableType> arrayHandle(std::move(storage));
    retVal = arrayHandle;
  }
  else if (shape.size() == 2)
  {
    switch(shape[1])
    {
      case 1:
        retVal =
          AllocateArrayHandle<VariableType, VariableType>(
            bufSize, buffer);
        break;
      case 2:
        retVal =
          AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 2> >(
            shape[0], buffer);
        break;
      case 3:
        retVal =
          AllocateArrayHandle<VariableType, vtkm::Vec<VariableType, 3> >(
            shape[0], buffer);
        break;
      default:
        break;
    }
  }
  else
  {
  // raise exception here
  }
  bpReader.Get(varADIOS2, buffer);

  return retVal;
}


template <typename VariableType>
std::vector<vtkm::cont::DynamicArrayHandle> ReadVariableBlocksInternal(
  adios2::IO& adiosIO,
  adios2::Engine& bpReader,
  const std::string& varName,
  const std::vector<size_t>& blocksToRead)
{
  auto varADIOS2 =
      adiosIO.InquireVariable<VariableType>(varName);
  // TODO: Fix the timestep which is the second argument
  // below
  auto blocksInfo =  bpReader.BlocksInfo(varADIOS2, 0);
  std::vector<size_t> blocksToReallyRead;
  if (blocksToRead.empty())
  {
    size_t nBlocks = blocksInfo.size();
    blocksToReallyRead.resize(nBlocks);
    std::iota(blocksToReallyRead.begin(),
              blocksToReallyRead.end(),
              0);
  }
  else
  {
    blocksToReallyRead = blocksToRead;
  }
  std::vector<vtkm::cont::DynamicArrayHandle> arrays;
  arrays.reserve(blocksToReallyRead.size());

  for(auto blockId : blocksToReallyRead)
  {
    varADIOS2.SetBlockSelection(blockId);
    arrays.push_back(
      ReadVariableInternal<VariableType>(bpReader, varADIOS2, blockId));
  }

  return arrays;
}

std::vector<vtkm::cont::DynamicArrayHandle>
  DataSource::ReadVariable(const std::string& varName,
                           const std::vector<size_t>& blocksToRead)
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variable without an open file.");
  }
  auto itr = this->AvailVars.find(varName);
  if (itr == this->AvailVars.end())
  {
    throw std::runtime_error("Variable " + varName + " was not found.");
  }
  const std::string& type = itr->second["Type"];

  if (type == "float")
  {
    return ReadVariableBlocksInternal<float>(
      this->AdiosIO, this->BpReader, varName, blocksToRead);
  }
  else  // if (type == "long long int")
  {
    return ReadVariableBlocksInternal<vtkm::Id>(
      this->AdiosIO, this->BpReader, varName, blocksToRead);
  }
}

void DataSource::DoAllReads()
{
  if(!this->Adios)
  {
    throw std::runtime_error("Cannot read variables without an open file.");
  }
  this->BpReader.PerformGets();
}


}
}