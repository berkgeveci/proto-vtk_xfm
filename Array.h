//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _Array_H_
#define _Array_H_

#include <vtkm/cont/DynamicArrayHandle.h>

#include "DataModel.h"

namespace adios2
{
namespace datamodel
{
struct Array : public DataModelBase
{
  std::vector<vtkm::cont::DynamicArrayHandle> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const std::vector<size_t>& blocksToRead);
};

}
}

#endif