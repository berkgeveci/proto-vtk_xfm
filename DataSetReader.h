//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _DataSetReader_h
#define _DataSetReader_h

#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/MultiBlock.h>

#include <memory>
#include <string>
#include <unordered_map>

#include "vtkm_xfm_export.h"

namespace vtkm
{
namespace io
{
namespace reader
{

class VTKM_XFM_EXPORT DataSetReader
{
public:
  DataSetReader(const std::string dataModelFilename);
  virtual ~DataSetReader();

  vtkm::cont::MultiBlock ReadDataSet(
    const std::unordered_map<std::string,
    std::string>& paths,
    const std::vector<size_t>& blocksToRead);

private:
  class DataSetReaderImpl;
  std::unique_ptr<DataSetReaderImpl> Impl;
};

} // end namespace reader
} // end namespace io
} // end namespace vtkm

#endif // _DataSetReader_h

