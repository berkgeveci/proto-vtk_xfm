//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _CoordinateSystem_H_
#define _CoordinateSystem_H_

#include "DataModel.h"
#include "Array.h"

#include <vtkm/cont/CoordinateSystem.h>

namespace adios2
{
namespace datamodel
{
struct CoordinateSystem : public DataModelBase
{
  virtual void ProcessJSON(const rapidjson::Value& json,
                           DataSourcesType& sources);

  std::vector<vtkm::cont::CoordinateSystem> Read(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const std::vector<size_t>& blocksToRead);

private:
  std::shared_ptr<adios2::datamodel::Array> Array;
};

}
}

#endif