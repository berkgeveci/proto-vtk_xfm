//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _DataModel_H_
#define _DataModel_H_

#include "DataSource.h"

#include <vtkm/cont/DynamicArrayHandle.h>

#include <rapidjson/document.h>

#include <string>
#include <unordered_map>
#include <vector>

namespace adios2
{
namespace datamodel
{

using DataSourceType = adios2::datamodel::DataSource;
using DataSourcesType =
  std::unordered_map<std::string, std::shared_ptr<DataSourceType> >;

struct DataModelBase
{
  DataModelBase() = default;
  DataModelBase(const DataModelBase &other)
  {
    if(this != &other)
    {
      this->DataSourceName = other.DataSourceName;
      this->VariableName = other.VariableName;
    }
  }

  virtual void ProcessJSON(const rapidjson::Value& json,
                           DataSourcesType& sources);

  std::string ObjectName = "";
  std::string DataSourceName = "";
  std::string VariableName = "";

protected:

  std::string FindDataSource(
    const rapidjson::Value& dataModel, DataSourcesType& sources) const;

  std::vector<vtkm::cont::DynamicArrayHandle> ReadSelf(
    const std::unordered_map<std::string, std::string>& paths,
    DataSourcesType& sources,
    const std::vector<size_t>& blocksToRead);
};

}
}

#endif