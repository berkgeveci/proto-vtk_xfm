//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#include "DataSetReader.h"

#include <mpi.h>
#include <string>
#include <unordered_map>
#include <vector>

int main(int argc, char** argv)
{
  MPI_Init(&argc, &argv);
  vtkm::io::reader::DataSetReader reader(argv[1]);
  std::unordered_map<std::string, std::string> paths;
  // paths["the-source"] = "tris.bp";
  paths["the-source"] = "tris-blocks.bp";
  reader.ReadDataSet(paths, std::vector<size_t>({0}));
  MPI_Finalize();
  return 0;
}