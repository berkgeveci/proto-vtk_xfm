//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//============================================================================

#ifndef _DataSource_H_
#define _DataSource_H_

#include <adios2.h>
#include <mpi.h>
#include <vector>

#include <vtkm/cont/DynamicArrayHandle.h>

namespace adios2
{
namespace datamodel
{

enum class FileNameMode
{
  Input,
  Relative
};

struct DataSource
{
  FileNameMode Mode;
  std::string FileName = "";

  DataSource() = default;
  DataSource& operator=(const DataSource& other)
  {
    if(this != &other)
    {
      this->Mode = other.Mode;
      this->FileName = other.FileName;
    }
    return *this;
  }

  DataSource(const DataSource &other)
  {
    if(this != &other)
    {
      this->Mode = other.Mode;
      this->FileName = other.FileName;
    }
  }

  void OpenSource(const std::string& fname);
  std::vector<vtkm::cont::DynamicArrayHandle> ReadVariable(
    const std::string& varName, const std::vector<size_t>& blocksToRead);
  void DoAllReads();

private:

  std::unique_ptr<adios2::ADIOS> Adios = nullptr;
  adios2::IO AdiosIO;
  adios2::Engine BpReader;
  enum class VarType
  {
    PointData,
    CellData
  };
  std::map<std::string, adios2::Params> AvailVars;
  std::map<std::string, adios2::Params> AvailAtts;
};

}
}

#endif
